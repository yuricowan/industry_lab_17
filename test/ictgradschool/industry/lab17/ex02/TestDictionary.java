package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import static ictgradschool.industry.lab17.ex02.IDictionary.WORDS;

/**
 * TODO Implement this class.
 */
public class TestDictionary {

    private Dictionary myTest;
    private Set<java.lang.String> dictionary = new TreeSet<java.lang.String>();
    private java.lang.String[] number = WORDS.toLowerCase().split(",");

    @Before
    public void setUp() {
        myTest = new Dictionary();
    }


    @Test
    public void testTrueIsTrue() {
        assertEquals(true, true);
    }

    @Test
    public void testDictonarySet() {
        for (int i = 0; i < number.length; i++) {
            dictionary.add(number[i]);
        }
    }

    @Test
    public void SpellingContains() {
        java.lang.String testing = number[(int) (Math.random() * number.length)];
        for (int i = 0; i < number.length; i++) {
            if (number[i].equals(testing)) {
                assertTrue(true);
                return;
            }
        }
        assertTrue(false);
    }

    @Test
    public void SpellingNotContains() {
        java.lang.String test = "Bod";
        for (int i = 0; i < number.length; i++) {
            if (number[i].equals(test)) {
                assertTrue(false);
                return;
            }
        }
        assertTrue(true);
    }

}