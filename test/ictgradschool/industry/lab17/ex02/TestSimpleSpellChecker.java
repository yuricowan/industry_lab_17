package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * TODO Implement this class.
 */
public class TestSimpleSpellChecker {
    private Map<String, Integer> userWords = new HashMap<>();
    private Dictionary dictionary = new Dictionary();
    private String wrong_word = "Testing here there their there";
    private String right_word = "testing";
    private String empty_word = null;
    private SimpleSpellChecker spellChecker;


    @Before
    public void setUp() {
        try {
            spellChecker = new SimpleSpellChecker(dictionary, wrong_word);
        } catch (InvalidDataFormatException e) {
            e.printStackTrace();
        }
        userWords.put("testing", 1);
        userWords.put("bod", 2);
    }

    @Test
    public void testFoundationsOfMathematics() {
        assertEquals(2, 1 + 1);
    }

    @Test
    public void testSimpleSpellCheckerNotWithin(){

        String[] words = wrong_word.split("[\\s\\W]+");
        for (int i = 0; i < words.length; i++) {
            Integer freq = userWords.get(words[i]);
            userWords.put(words[i].toLowerCase(), (freq == null) ? 1 : freq + 1);
        }
        for (int i = 0; i < words.length; i++) {
            if (!(userWords.get(words[i].toLowerCase())> 0)){
                fail();
            }
        }
        assertTrue(true);
    }

    @Test
    public void testSimpleSpellCheckerWithin(){
        String[] words = right_word.split("[\\s\\W]+");
        for (int i = 0; i < words.length; i++) {
            Integer freq = userWords.get(words[i]);
            userWords.put(words[i].toLowerCase(), (freq == null) ? 1 : freq + 1);
        }
        for (int i = 0; i < words.length; i++) {
            if (userWords.get(words[i].toLowerCase())> 1){
                assertTrue(true);
                return;
            }
        }
        fail();
    }

    @Test
    public void testSimpleSpellCheckerNull(){
        if (empty_word == null) {
            assertTrue(true);
        }
        else{
            fail();
        }
    }

    @Test
    public void testMissSpellingWords(){
        assertTrue(spellChecker.getMisspelledWords().size()>=0);
    }

    @Test
    public void testGetUniqueWords(){
        assertTrue(spellChecker.getUniqueWords().size() == 4);
    }

    @Test
    public void testGetFrequencyOfWord(){
        try {
            assertTrue(spellChecker.getFrequencyOfWord("there") == 2);
        } catch (InvalidDataFormatException e) {
            fail();
        }
    }
}