package ictgradschool.industry.lab17.ex03;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by ycow194 on 23/05/2017.
 */
public class TestGemShape {
    private MockPainter painter;

    @Before
    public void setUp() {
        painter = new MockPainter();
    }

    public static String pointsDisplay(GemShape gemShape){
        List<Integer> x_points = new ArrayList<>();
        List<Integer> y_points = new ArrayList<>();

        if (gemShape.getWidth() <= 40) {
            x_points.add(gemShape.fX);
            y_points.add(gemShape.fY + gemShape.fHeight / 2);
            x_points.add(gemShape.fX + gemShape.fWidth / 2);
            y_points.add(gemShape.fY);
            x_points.add(gemShape.fX + gemShape.fWidth);
            y_points.add( gemShape.fY + gemShape.fHeight / 2);
            x_points.add(gemShape.fX + gemShape.fWidth / 2);
            y_points.add(gemShape.fY + gemShape.fHeight);

        }

        else {
            x_points.add(gemShape.fX);
            y_points.add(gemShape.fY + gemShape.fHeight / 2);
            x_points.add(gemShape.fX + 20);
            y_points.add(gemShape.fY);
            x_points.add(gemShape.fX + gemShape.fWidth - 20);
            y_points.add( gemShape.fY);
            x_points.add(gemShape.fX + gemShape.fWidth);
            y_points.add( gemShape.fY + gemShape.fHeight / 2);
            x_points.add(gemShape.fX + gemShape.fWidth - 20);
            y_points.add( gemShape.fY + gemShape.fHeight);
            x_points.add(gemShape.fX + 20);
            y_points.add(gemShape.fY + gemShape.fHeight);
        }

        String statment = "(polygon xpoints: [";
        for (Integer x_point : x_points) {
            statment += x_point+", ";
        }
        statment = statment.trim().substring(0,statment.length()-2) +"], ypoints: [";
        for (Integer y_point : y_points) {
            statment += y_point+", ";
        }
        statment = statment.trim().substring(0,statment.length()-2) +"])";
        return statment;
    }
    /**
     * Tests whether the {@link GemShape}'s default constructor functions as expected.
     */
    @Test
    public void testDefaultConstructor() {
        GemShape shape = new GemShape();

        assertEquals(0, shape.getX());
        assertEquals(0, shape.getY());
        assertEquals(5, shape.getDeltaX());
        assertEquals(5, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        // Check that the paint method caused a rectangle at position (0, 0), with size (25, 35), to be drawn.
        shape.paint(painter);
        assertEquals(pointsDisplay(shape), painter.toString());
    }

    /**
     * Tests whether the {@link GemShape}'s constructor which takes speed arguments functions as expected.
     */
    @Test
    public void testConstructorWithSpeedValues() {
        GemShape shape = new GemShape(1, 2, 3, 4);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        shape.paint(painter);
        assertEquals(pointsDisplay(shape), painter.toString());
    }

    /**
     * Tests whether the {@link GemShape}'s constructor which takes all arguments functions as expected.
     */
    @Test
    public void testConstructorWithAllValues() {
        GemShape shape = new GemShape(1, 2, 3, 4, 5, 6);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(5, shape.getWidth());
        assertEquals(6, shape.getHeight());

        shape.paint(painter);
        assertEquals(pointsDisplay(shape), painter.toString());
    }

    @Test
    public void testSixSidedValues() {
        GemShape shape = new GemShape(1, 2, 3, 4, 50, 50);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [1, 21, 31, 51, 31, 21, 0, 0], ypoints: [27, 2, 2, 27, 52, 52, 0, 0])", painter.toString());
    }

    /**
     * Tests whether just moving a {@link GemShape} works, with no bouncing involved.
     */
    @Test
    public void testSimpleMove() {
        GemShape shape = new GemShape(100, 20, 12, 15);
        String one = pointsDisplay(shape);
        shape.paint(painter);
        shape.move(500, 500);
        one += pointsDisplay(shape);
        shape.paint(painter);

        // Checks that two rectangles were drawn (one for each call to "paint").
        // The first one should be at the initial position (100, 20), and the second one should be at
        // position (112, 35) (i.e. x + deltaX, y + deltaY). The width and height should be (25, 35) in both cases.
        assertEquals(one,
                painter.toString());
    }

    /**
     * Tests whether moving a {@link GemShape} works, when it bounces off the right wall.
     */
    @Test
    public void testShapeMoveWithBounceOffRight() {
        GemShape shape = new GemShape(100, 20, 12, 15);
        String one = pointsDisplay(shape);
        shape.paint(painter);
        shape.move(135, 10000);
        one += pointsDisplay(shape);
        shape.paint(painter);
        shape.move(135, 10000);
        one += pointsDisplay(shape);
        shape.paint(painter);

        // Checks that three rectangles were drawn (one for each call to "paint").
        // The first one should be at the initial position (100, 20). The second one should be at position (110, 35)
        // because it hit the right wall (110 is the largest possible x value since the width of the space the shape
        // is moving in is 135, and 135 (the area's width) - 25 (the shape's width) = 110).
        // The third rectangle should be at position (98, 50) because the deltaX should have reversed as the shape bounced,
        // and it should have moved 12 pixels to the left.
        assertEquals(one, painter.toString());
    }

    /**
     * Tests whether moving a {@link GemShape} works, when it bounces off the left wall.
     */
    @Test
    public void testShapeMoveWithBounceOffLeft() {
        GemShape shape = new GemShape(10, 20, -12, 15);
        String one = pointsDisplay(shape);
        shape.paint(painter);
        shape.move(10000, 10000);
        one += pointsDisplay(shape);
        shape.paint(painter);
        shape.move(10000, 10000);
        one += pointsDisplay(shape);
        shape.paint(painter);
        assertEquals(one, painter.toString());
    }


    /**
     * Tests whether moving a {@link GemShape} works, when it bounces off the top wall.
     */
    @Test
    public void testShapeMoveWithBounceOffTop() {
        GemShape shape = new GemShape(10, 10, 0, -15);
        String one = pointsDisplay(shape);
        shape.paint(painter);
        shape.move(10000, 10000);
        one += pointsDisplay(shape);
        shape.paint(painter);
        shape.move(10000, 10000);
        one += pointsDisplay(shape);
        shape.paint(painter);
        assertEquals(one, painter.toString());
    }

    /**
     * Tests whether moving a {@link GemShape} works, when it bounces off the bottom wall.
     */
    @Test
    public void testShapeMoveWithBounceOffBottom() {
        GemShape shape = new GemShape(10, 20, 0, 15);
        String one = pointsDisplay(shape);
        shape.paint(painter);
        shape.move(10000, 60);
        one += pointsDisplay(shape);
        shape.paint(painter);
        shape.move(10000, 60);
        one += pointsDisplay(shape);
        shape.paint(painter);
        assertEquals(one, painter.toString());
    }

    /**
     * Tests whether moving a {@link GemShape} works, when it bounces off the bottom-left corner.
     */
    @Test
    public void testShapeMoveWithBounceOffBottomAndLeft() {
        GemShape shape = new GemShape(10, 90, -12, 15);
        String one = pointsDisplay(shape);
        shape.paint(painter);
        shape.move(125, 135);
        one += pointsDisplay(shape);
        shape.paint(painter);
        shape.move(125, 135);
        one += pointsDisplay(shape);
        shape.paint(painter);
        assertEquals(one, painter.toString());
    }

    /**
     * Tests whether moving a {@link GemShape} works, when it bounces off the bottom-right corner.
     */
    @Test
    public void testShapeMoveWithBounceOffBottomAndRight() {
        GemShape shape = new GemShape(70, 60, 10, 10);
        String one = pointsDisplay(shape);
        shape.paint(painter);
        shape.move(100, 100);
        one += pointsDisplay(shape);
        shape.paint(painter);
        shape.move(100, 100);
        one += pointsDisplay(shape);
        shape.paint(painter);
        assertEquals(one, painter.toString());
    }

    /**
     * Tests whether moving a {@link GemShape} works, when it bounces off the top-right corner.
     */
    @Test
    public void testShapeMoveWithBounceOffTopAndRight() {
        GemShape shape = new GemShape(70, 5, 10, -10);
        String one = pointsDisplay(shape);
        shape.paint(painter);
        shape.move(100, 100);
        one += pointsDisplay(shape);
        shape.paint(painter);
        shape.move(100, 100);
        one += pointsDisplay(shape);
        shape.paint(painter);
        assertEquals(one, painter.toString());
    }

    /**
     * Tests whether moving a {@link GemShape} works, when it bounces off the top-left corner.
     */
    @Test
    public void testShapeMoveWithBounceOffTopAndLeft() {
        GemShape shape = new GemShape(5, 5, -10, -10);
        String one = pointsDisplay(shape);
        shape.paint(painter);
        shape.move(100, 100);
        one += pointsDisplay(shape);
        shape.paint(painter);
        shape.move(100, 100);
        one += pointsDisplay(shape);
        shape.paint(painter);
        assertEquals(one, painter.toString());
    }
}
