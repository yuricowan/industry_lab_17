package ictgradschool.industry.lab17.ex04;

import org.junit.*;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ycow194 on 25/05/2017.
 */
public class TestEx04Testerino {

    private ex04Testerino testing;

    @Before
    public void setUp() {
        testing = new ex04Testerino();
    }

    @Test
    public void testStringtoDouble() {
        try {
            assertEquals(123.123, testing.ConvertionStringToDouble("123.123"), 1e-15);
            assertEquals(23.32, testing.ConvertionStringToDouble("23.32"), 1e-15);
            assertEquals(0.0, testing.ConvertionStringToDouble("0"), 1e-15);
            assertEquals(10.00, testing.ConvertionStringToDouble("10"), 1e-15);
        } catch (NumberFormatException e) {
            fail();
        }
        try {
            testing.ConvertionStringToDouble("heeloooo");
            fail();
        } catch (NumberFormatException e) {
            assertTrue(true);
        }
    }

    @Test
    public void testrecangle() {
        try {
            assertEquals(6.25, testing.rectangle(2.5, 2.5), 1e-15);
            assertEquals(29.7587, testing.rectangle(5.23, 5.69), 1e-9);
            assertEquals(100, testing.rectangle(10, 10), 1e-15);
            assertEquals(0, testing.rectangle(0, 0), 1e-15);
            assertEquals(10.00, testing.rectangle(10.0, 1), 1e-15);
        } catch (MyExceptions e) {
            fail();
        }
        try {
            assertEquals(-1.25, testing.rectangle(-2.5, -2.5), 1e-15);
            fail();
        } catch (MyExceptions e) {
            assertTrue(true);
        }
    }

    @Test
    public void testTriangle() {
        try {
            assertEquals(19.634954084936207740391521145497, testing.triangle(2.5), 1e-15);
            assertEquals(85.93166969437627, testing.triangle(5.23), 1e-50);
            assertEquals(314.15926535897932384626433832795, testing.triangle(10), 1e-15);
            assertEquals(314.15926535897932384626433832795, testing.triangle(10.0), 1e-15);
            assertEquals(0, testing.triangle(0), 1e-15);
        } catch (MyExceptions expections) {
            fail();
        }
        try {
            assertEquals(-1.25, testing.triangle(-10.0), 1e-15);
            fail();
        } catch (MyExceptions expections) {
            assertTrue(true);
        }

    }

    @Test
    public void testRound() {
        try {
            assertEquals(20, testing.rounding(2.5), 1e-15);
            assertEquals(86, testing.rounding(5.23), 1e-50);
            assertEquals(314, testing.rounding(10), 1e-15);
            assertEquals(314, testing.rounding(10.0), 1e-15);
            assertEquals(0, testing.rounding(0), 1e-15);
        } catch (MyExceptions expections) {
            fail();
        }
        try {
            assertEquals(314, testing.rounding(-10.0), 1e-15);
            fail();
        } catch (MyExceptions expections) {
            assertTrue(true);
        }

    }

    @Test
    public void testRoundRec() {
        try {
            assertEquals(6, testing.roundingRec(2.5, 2.5), 1e-15);
            assertEquals(30, testing.roundingRec(5.23, 5.69), 1e-50);
            assertEquals(100, testing.roundingRec(10, 10), 1e-15);
            assertEquals(10, testing.roundingRec(10.0, 1), 1e-15);
            assertEquals(0, testing.roundingRec(0, 1), 1e-15);
        } catch (MyExceptions expections) {
            fail();
        }
        try {
            assertEquals(10, testing.roundingRec(-10.0, 1), 1e-15);
            fail();
        } catch (MyExceptions expections) {
            assertTrue(true);
        }
        try {
            assertEquals(10, testing.roundingRec(10.0, -1), 1e-15);
            fail();
        } catch (MyExceptions expections) {
            assertTrue(true);
        }
    }


    @Test
    public void testSmaller() {
        try {
            assertTrue(testing.comparingSmall(testing.roundingRec(2.5, 2.5), testing.rounding(2.5)));
            assertFalse(testing.comparingSmall(testing.rounding(2.5), testing.roundingRec(2.5, 2.5)));
            assertTrue(testing.comparingSmall(testing.roundingRec(10, 10), testing.rounding(10.0)));
            assertFalse(testing.comparingSmall(testing.rounding(2.5), testing.roundingRec(2.5, 2.5)));
        } catch (MyExceptions expections) {
            fail();
        }
        try {
            assertTrue(testing.comparingSmall(testing.roundingRec(-10, -10), testing.rounding(10.0)));
            fail();
        } catch (MyExceptions expections) {
            assertTrue(true);
        }
        try {
            assertFalse(testing.comparingSmall(testing.rounding(-2.5), testing.roundingRec(2.5, -2.5)));
            fail();
        } catch (MyExceptions expections) {
            assertTrue(true);
        }
        try {
            assertFalse(testing.comparingSmall(testing.rounding(2.5), testing.roundingRec(-2.5, 2.5)));
            fail();
        } catch (MyExceptions expections) {
            assertTrue(true);
        }
        try {
            assertFalse(testing.comparingSmall(testing.rounding(2.5), testing.roundingRec(2.5, -2.5)));
            fail();
        } catch (MyExceptions expections) {
            assertTrue(true);
        }
    }


    @Test
    public void testReturningSmaller() {
        try {
            assertEquals(379, testing.smaller(20, 18.5, 20.5));
            assertEquals(400, testing.smaller(20, 20, 20));
            assertEquals(1257, testing.smaller(20, 100, 50));
        } catch (MyExceptions expections) {
            fail();
        }
        try {
            assertEquals(400, testing.smaller(-20, 20, 20));
        } catch (MyExceptions expections) {
            assertTrue(true);
        }
        try {
            assertEquals(1257, testing.smaller(20, -100, -50));
            fail();
        } catch (MyExceptions expections) {
            assertTrue(true);
        }
    }

    @After
    public void checkList() {
        System.out.println(this);
    }
}

