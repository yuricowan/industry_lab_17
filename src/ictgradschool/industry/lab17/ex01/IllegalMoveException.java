package ictgradschool.industry.lab17.ex01;


public class IllegalMoveException extends Exception {

//   Robots can move one cell towards in the direction that they are facing as long as this wouldn’t put them outside their world
//   Robots can turn 90 degrees right
//   Robots remember their moves and are able to backtrack


//    Constructor
//    Test that a Robot instance is created facing the prescribed direction and in the specified cell
//=> 1 test case





//    Method turn
//    Test right turn for each direction the Robot is facing
//=> 4 test cases







//    Method move
//    Test for a valid move in each direction the Robot is facing
//    Test for an invalid move (i.e. an attempt to move outside of the 2D grid) in each direction the Robot is facing
//=> 8 test cases



}
