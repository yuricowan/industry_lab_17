package ictgradschool.industry.lab17.ex04;

import ictgradschool.Keyboard;

/**
 * Created by ycow194 on 25/05/2017.
 */
public class ex04Testerino {
    public static double ConvertionStringToDouble(String number) {
        return Double.parseDouble(number);
    }

    public double rectangle(double length, double width) throws MyExceptions {
        if (length < 0 || width < 0) {
            throw new MyExceptions();
        }
        return length * width;
    }

    public double triangle(double radius) throws MyExceptions {
        if (radius < 0) {
            throw new MyExceptions();
        }
        return Math.pow(radius, 2) * Math.PI;
    }

    public int rounding(double results) throws MyExceptions {
        return Math.round((float) triangle(results));
    }

    public int roundingRec(double length, double width) throws MyExceptions {
        if (length < 0 || width < 0) {
            throw new MyExceptions();
        }
        return Math.round((float) rectangle(length, width));
    }

    public boolean comparingSmall(double one, double two) {
        return one < two;
    }

    public int smaller(double one, double two, double three) throws MyExceptions {
        if (one < 0 || two < 0 || three < 0) {
            throw new MyExceptions();
        }
        int smaller = (rectangle(two, three) < triangle(one)) ? roundingRec(two, three) : rounding(one);
        return smaller;
    }

    public static void main(String[] args) {
        ex04Testerino testing = new ex04Testerino();
        System.out.println("Welcome to Shape Area Calculator!");
        System.out.println();
        System.out.print("Enter the width of the rectangle: ");
        double number1 = ConvertionStringToDouble(Keyboard.readInput());
        System.out.print("Enter the length of the rectangle: ");
        double number2 = ConvertionStringToDouble(Keyboard.readInput());
        System.out.println();
        System.out.print("The radius of the circle is: ");
        System.out.println();
        double number3 = ConvertionStringToDouble(Keyboard.readInput());
        try {
            System.out.print("The smaller area is: " + testing.smaller(number3, number1, number2));
        } catch (MyExceptions myExceptions) {
            myExceptions.printStackTrace();
        }

    }

}
